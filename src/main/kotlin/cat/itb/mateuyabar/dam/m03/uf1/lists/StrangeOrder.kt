package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    // 1 2 3 -1
    val scanner = Scanner(System.`in`)
    val list = mutableListOf<Int>()
    var number = scanner.nextInt()
    var addToStart = true
    while(number!=-1){
        if(addToStart)
            list.add(0, number)
        else
            list += number
        number = scanner.nextInt()
        addToStart = !addToStart
    }
    println(list)
}