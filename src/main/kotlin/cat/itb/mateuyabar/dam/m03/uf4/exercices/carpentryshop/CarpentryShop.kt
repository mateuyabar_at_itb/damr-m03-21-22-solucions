package cat.itb.mateuyabar.dam.m03.uf4.exercices.carpentryshop

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)

    val products = readProducts(scanner)
    val price = sumPrices(products)
    println("El preu total és: $price€")
}

fun readProducts(scanner: Scanner): List<Product> {
    val count = scanner.nextInt()
    val products = List(count){
        readProduct(scanner)
    }
    return products

}

fun sumPrices(products: List<Product>): Int {
    var sum = 0
    for(product in products)
        sum += product.totalPrice
    return sum
}

fun readProduct(scanner: Scanner) : Product{
    val type = scanner.next()
    return when(type){
        "Taulell" -> readTaulell(scanner)
        else -> readLlisto(scanner)
    }
}

fun readLlisto(scanner: Scanner) =
    Llisto(scanner.nextInt(), scanner.nextInt())

fun readTaulell(scanner: Scanner) =
    Taulell(scanner.nextInt(), scanner.nextInt(), scanner.nextInt())
