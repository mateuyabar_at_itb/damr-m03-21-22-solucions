package cat.itb.mateuyabar.dam.m03.uf1.data

import java.util.*

fun isValidNote(ammount: Int): Boolean{
    val is5Note = ammount == 5
    val is10Note = ammount == 10
    val is20Note = ammount == 20
    val is50Note = ammount == 50
    val is100Note = ammount == 100
    val is200Note = ammount == 200
    val is500Note = ammount == 500
    return is5Note || is10Note || is20Note || is50Note || is100Note|| is200Note|| is500Note
}

fun main() {
    val scanner = Scanner(System.`in`)
    val ammount = scanner.nextInt()

    val isValidNote = isValidNote(ammount)

    println(isValidNote)
}