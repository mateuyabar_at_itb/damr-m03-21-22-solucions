package cat.itb.mateuyabar.dam.m03.uf4.exercices.motto

fun main() {
    val teams = listOf(
        BasketballTeam("Mosques", "Bzzzanyarem"),
        VolleyballTeam("Dragons", "Grooarg", "verd"),
        GolfTeam("Abelles", "Piquem Fort", "Ko Jin-Young"))
    shoutMottos(teams)
}
