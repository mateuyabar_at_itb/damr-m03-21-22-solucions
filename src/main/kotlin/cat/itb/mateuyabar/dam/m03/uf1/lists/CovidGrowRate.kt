package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val cases = readIntList(scanner)
    // 3 10
    for(i in 1..cases.lastIndex){
        val rate = cases[i].toDouble()/cases[i-1]
        print(rate)
    }
}