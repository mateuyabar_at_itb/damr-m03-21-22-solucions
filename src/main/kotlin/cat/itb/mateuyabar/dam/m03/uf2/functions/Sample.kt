package cat.itb.mateuyabar.dam.m03.uf2.functions


data class Rectangle(var width: Double, var height: Double){
    val area get() = width * height

    fun toOutput()="Rectangle de $width per $height i area $area"

    fun zoomBy(factor:Int){
        width*=factor
        height*=factor
    }
}


fun main() {
    val rec = Rectangle(2.0,3.0)
    println(rec)
    rec.zoomBy(5)
    println(rec.toOutput())



}