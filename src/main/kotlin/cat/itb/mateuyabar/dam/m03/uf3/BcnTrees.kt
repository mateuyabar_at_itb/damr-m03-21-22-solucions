package cat.itb.mateuyabar.dam.m03.uf3

import kotlinx.serialization.SerialName
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.nio.file.Path
import java.util.*
import kotlin.io.path.readText

@kotlinx.serialization.Serializable
data class BcnTree(@SerialName("nom_cientific") val name : String)

fun main() {
    val trees : List<BcnTree> = readTrees()
    val scanner = Scanner(System.`in`)
    val nameToFind = scanner.nextLine()
    val count = countTrees(trees, nameToFind)
    println(count)
}

fun readTrees(): List<BcnTree> {
    val treeFile = Path.of("OD_Arbrat_Zona_BCN.json")
    return Json.decodeFromString(treeFile.readText())
}

fun countTrees(trees: List<BcnTree>, nameToFind: String?):Int {
    var count = 0
    trees.forEach{
        if(it.name==nameToFind) count++
    }
    return count
}
