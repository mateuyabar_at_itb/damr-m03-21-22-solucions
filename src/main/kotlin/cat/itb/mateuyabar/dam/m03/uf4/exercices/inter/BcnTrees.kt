package cat.itb.mateuyabar.dam.m03.uf4.exercices.inter

import cat.itb.mateuyabar.dam.m03.uf3.BcnTree

interface BcnTreesSource{
    suspend fun list() : List<BcnTree>
}
class ApiBcnTreeSource : BcnTreesSource{
    override suspend fun list(): List<BcnTree> {
        TODO()
    }
}
class FileBcnTreeSource : BcnTreesSource{
    override suspend fun list(): List<BcnTree> {
        TODO()
    }
}
suspend fun main() {
    var useNetwork = true
    val bcnTreesSource = if(useNetwork) ApiBcnTreeSource() else FileBcnTreeSource()
    val trees = bcnTreesSource.list()
    println(trees)
}