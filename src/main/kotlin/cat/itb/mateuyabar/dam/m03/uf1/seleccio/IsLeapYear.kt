package cat.itb.mateuyabar.dam.m03.uf1.seleccio

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val year = scanner.nextInt()
    val isLeapYear = if(year%4==0){
        if(year%100==0) {  // acaba en 00
            val first2Digits = year/100
            first2Digits%4==0
        } else {
            true
        }
    } else {
        false
    }

    if(isLeapYear)
        println("$year és any de traspàs")
    else
        println("$year no és any de traspàs")

}