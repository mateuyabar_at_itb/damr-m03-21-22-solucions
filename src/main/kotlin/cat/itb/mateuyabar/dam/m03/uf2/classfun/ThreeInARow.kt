package cat.itb.mateuyabar.dam.m03.uf2.classfun

import cat.itb.mateuyabar.dam.m03.uf1.data.project.scanner
import java.util.*

data class ThreeInARow(
    val board : List<MutableList<Int>> = List(3){MutableList(3){0} },
    var currentPlayer :Int = 1
) {
    /**
     * Returns the board as a string
     * Example:
     * X · 0
     * · X ·
     * · 0 ·
     */
    fun boardToString() : String {
        var result = ""
        for(i in board.indices){
            val row = board[i]
            for(i in row.indices){
                val cell = row[i]
                val text = playerToString(cell)
                result+="$text"
                if(i<row.lastIndex) result+=" "
            }
            if(i<board.lastIndex)
                result +="\n"
        }
        return result
    }

    private fun playerToString(cell: Int?) = when(cell){
        1-> "X"
        2-> "0"
        else -> "·"

    }

    /**
     * Depending on the currentPlayer,
     * puts a 1 or 0 at the board in the
     * current position
     */
    fun playAt(x: Int, y: Int){
        board[x][y] = currentPlayer
        currentPlayer = currentPlayer%2 +1
    }

    /**
     * Returns the number of the player that has
     * played at position x,y, or 0 if noone has
     * played there yet
     */
    fun valueAt(x: Int, y: Int) = board[x][y]

    fun winner():Int?{
        return winnerByRow() ?: winnerByColumn() ?: winnerByDiagonal()
    }

    private fun winnerByDiagonal(): Int? {
        if(board[0][0]!=0 && board[0][0]==board[1][1] && board[0][0]==board[2][2]){
            return board[1][1]
        }
        if(board[2][0]!=0 && board[2][0]==board[1][1] && board[1][1]==board[0][2]){
            return board[1][1]
        }
        return null
    }

    private fun winnerByRow(): Int? {
        for(row in board) {
            if (row[0]!=0 && row[0] == row[1] && row[1] == row[2]) {
                return row[0]
            }
        }
        return null
    }

    private fun winnerByColumn(): Int? {
        for(i in 0..2) {
            if (board[0][i]!=0 && board[0][i] == board[1][i] && board[1][i] == board[2][i]) {
                return board[0][i]
            }
        }
        return null
    }

    fun fullBoard() : Boolean {
        for(row in board){
            for(cell in row){
                if(cell==0)
                    return false
            }
        }
        return true
    }

    fun finished() = fullBoard() || winner()!=null

    fun winnerAsString() = playerToString(winner())
}

fun main() {
    threeInARow()
}

fun threeInARow() {
    val scanner = Scanner(System.`in`)
    val threeInARow = ThreeInARow()
    playGame(scanner, threeInARow)
}

fun playGame(scanner: Scanner, threeInARow: ThreeInARow) {
    while(!threeInARow.finished()){
        playTurn(scanner, threeInARow)
        println(threeInARow.boardToString())
    }
    if(threeInARow.fullBoard()){
        println("empat")
    } else {
        println("guanyen les ${threeInARow.winnerAsString()}")
    }
}

fun playTurn(scanner: Scanner, threeInARow: ThreeInARow) {
    val x = scanner.nextInt()
    val y = scanner.nextInt()
    threeInARow.playAt(x,y)
}
