package cat.itb.mateuyabar.dam.m03.uf1.data

import java.util.*

fun main() {
    // llegir el nom
    val scanner = Scanner(System.`in`)
    val name = scanner.nextLine()
    // imprimir bon dia
    println("Bon dia $name")
}