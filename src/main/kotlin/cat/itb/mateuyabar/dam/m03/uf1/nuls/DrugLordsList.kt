package cat.itb.mateuyabar.dam.m03.uf1.nuls

import java.util.*


data class DrugLord(val name: String, var surname: String?, var birthYear : Int?)

fun main() {
    val scanner = Scanner(System.`in`)
    val size = scanner.nextInt()
    scanner.nextLine()

    val list = List(size){
        DrugLord(scanner.nextLine(), null, null)
    }
    for(drugLord in list){
        drugLord.surname = scanner.nextLine()
    }
    for(drugLord in list){
        drugLord.birthYear = scanner.nextInt()
    }
    TODO("print list")
}