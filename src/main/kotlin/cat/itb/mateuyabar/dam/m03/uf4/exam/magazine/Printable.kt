

interface Printable{
    /**
     * Prints the printable to the printer
     */
    fun print(magazinePrinter: MagazinePrinter)
}