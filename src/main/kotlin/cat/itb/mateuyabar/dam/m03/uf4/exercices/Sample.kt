package cat.itb.mateuyabar.dam.m03.uf4.exercices

import java.util.LinkedList

//class Person(val name: String, val subscribes : List<Magazine>)
//class Magazine(val name: String)

//class Person(val name: String)
//class Magazine(val name: String, val subscrived : List<Person>)

data class Person(val name: String):Comparable<Person>{
    override fun compareTo(other: Person): Int {
        return name.compareTo(other.name)
    }

}

fun main() {
    val list = mutableListOf(Person("Iu"), Person("Mar"), Person("Ona"), Person("Jan"))
    list.sort()
    println(list)
}