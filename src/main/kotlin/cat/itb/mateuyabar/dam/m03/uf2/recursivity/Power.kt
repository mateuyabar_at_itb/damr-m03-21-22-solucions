package cat.itb.mateuyabar.dam.m03.uf2.recursivity

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val num = scanner.nextLong()
    val exp = scanner.nextLong()
    println(power(num, exp))
}

fun power(num: Long, exp: Long): Long {
    return if(exp==0L)
        1
    else
        power(num, exp-1)*num
}
