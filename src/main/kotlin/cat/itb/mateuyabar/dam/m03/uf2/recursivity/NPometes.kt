package cat.itb.mateuyabar.dam.m03.uf2.recursivity

fun getAppleSongStanza(applesCount: Int) =
    """
        $applesCount pometes té el pomer, 
        de $applesCount una, de $applesCount una,
        $applesCount pometes té el pomer,
        de $applesCount una en caigué. 
        Si mireu el vent d'on vé 
        veureu el pomer com dansa, 
        si mireu el vent d'on vé
        veureu com dansa el pomer.
    """

fun getAppleSong(applesCount: Int):String{
    val stanza = getAppleSongStanza(applesCount)
//    if(applesCount==1)
//        return stanza
    val nextSong = getAppleSong(applesCount-1)
    return stanza + nextSong

}

fun main() {
    println(getAppleSong(4))
}

