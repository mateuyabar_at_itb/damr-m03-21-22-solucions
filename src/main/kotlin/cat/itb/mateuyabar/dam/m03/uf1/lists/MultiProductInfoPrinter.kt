package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

data class Product(val name : String, val price : Double)

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val products = List(scanner.nextInt()){
        val name = scanner.next()
        val price = scanner.nextDouble()
        Product(name, price)
    }
    for(product in products){
        println("El producte ${product.name} val ${product.price}€")
    }
}