package cat.itb.mateuyabar.dam.m03.uf2.modularitat

class Board(val size:Int,
    val bombCount : Int,
    val matrix : List<List<Cell>> = MutableList(size) { i -> MutableList(size) { j -> Cell(i, j) } }) {

    fun generateBombsList(){
        matrix.flatten().shuffled().take(5)

    }

    fun calculateAdjacencies(){
        matrix[3][3].adjacentBombs = TODO()
    }
}