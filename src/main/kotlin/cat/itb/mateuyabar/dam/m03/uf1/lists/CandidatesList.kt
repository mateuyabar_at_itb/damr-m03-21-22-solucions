package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    // llegir llista
    val size = scanner.nextInt()
    scanner.nextLine()
    val candidates = List(size){
        scanner.nextLine()
    }

    // mostrar el candidat en la posició n
    var position = scanner.nextInt()
    while(position!=-1) {
        println(candidates[position-1])
        position = scanner.nextInt()
    }
}