package cat.itb.mateuyabar.dam.m03.uf1.seleccio

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val month = scanner.nextInt()
    val days = when(month){
        1,3,5,7,8,10,12->31
        2->28
        4,6,9,11 -> 30
        else -> 0
    }
    println(days)
}