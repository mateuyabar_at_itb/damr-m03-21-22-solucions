package cat.itb.mateuyabar.dam.m03.uf1.seleccio

import cat.itb.mateuyabar.dam.m03.uf1.data.isValidNote
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val ammount = scanner.nextInt()
    if(isValidNote(ammount))
        println("bitllet vàlid")
    else
        println("bitllet invàlid")
}