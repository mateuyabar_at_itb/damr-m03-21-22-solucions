package cat.itb.mateuyabar.dam.m03.uf2.functions

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val grades = readIntList(scanner)
    val minGrade = min(grades)
    val maxGrade = max(grades)
    val avgGrade = avg(grades)
    println("Nota mínima:  $minGrade")
    println("Nota màxima:  $maxGrade")
    println("Nota mitjana: $avgGrade")
}