package cat.itb.mateuyabar.dam.m03.uf3

import java.nio.file.Path
import java.util.*
import kotlin.io.path.*

fun main() {
    val scanner = Scanner(System.`in`)
    val path = Path.of(scanner.nextLine())
    for(subPath in path.listDirectoryEntries()){
        val newName = subPath.name.uppercase()
        val newPath = subPath.parent.resolve(newName)
        subPath.moveTo(newPath)
    }
}