package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val prices = List(10){
        scanner.nextDouble()
    }
    for(price in prices){
        val totalPrice = price*1.21
        println("$price IVA = $totalPrice")
    }
}