package cat.itb.mateuyabar.dam.m03.uf2.recursivity

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val num = scanner.nextInt()
    println(figures(num))
}

fun figures(num: Int): Int {
    if(num<10)
        return 1
    else
        return figures(num/10)+1

}
