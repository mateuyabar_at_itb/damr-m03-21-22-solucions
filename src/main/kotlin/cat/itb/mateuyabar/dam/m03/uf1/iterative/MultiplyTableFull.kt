package cat.itb.mateuyabar.dam.m03.uf1.iterative

fun main() {
    for(i in 1..9) {
        for (j in 1..9) {
            val result = i*j
            if(result<10)
                print(" ")
            print("$result ")
        }
        println()
    }


}