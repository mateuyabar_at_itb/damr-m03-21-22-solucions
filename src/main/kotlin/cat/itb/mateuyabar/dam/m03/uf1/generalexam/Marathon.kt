package cat.itb.mateuyabar.dam.m03.uf1.generalexam


import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val runners = List(50){
        scanner.nextInt()
    }
    var teams = MutableList(10){0}

    for(i in runners.indices){
        val team = i%10
        teams[team]+=runners[i]
    }
    println(teams)
}