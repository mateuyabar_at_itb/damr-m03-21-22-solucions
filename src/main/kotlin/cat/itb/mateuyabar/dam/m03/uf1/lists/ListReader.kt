package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

/**
 * Reads an int of lists from user input.
 * The user first introduces the number (N).
 * Later it introduces the integers one by one.
 * @return int list of values introduced of size N
 */
fun readIntList(scanner: Scanner):List<Int>{
    val size = scanner.nextInt()
    return List(size){scanner.nextInt()}
}

fun main() {
    val scanner = Scanner(System.`in`)
    val list = readIntList(scanner)
}