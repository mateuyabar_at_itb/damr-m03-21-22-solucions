package cat.itb.mateuyabar.dam.m03.uf3

import kotlin.io.path.Path
import kotlin.io.path.createDirectories

fun main() {
    val homePath = System.getProperty("user.home")
    val folder = Path(homePath, "dummyfolders")
    for(i in 1..100) {
        val subFolder = folder.resolve(i.toString())
        subFolder.createDirectories()
    }
}