package cat.itb.mateuyabar.dam.m03.uf5.exercices.tads.maps

import java.util.*

data class Employee(val dni: String, val name: String,
    val surname: String, val address: String)

fun main() {
    val scanner = Scanner(System.`in`)
    val employees = readEmployees(scanner)
    processQueries(scanner, employees)
}

fun processQueries(scanner: Scanner, employees: Map<String, Employee>) {
    var dni = scanner.nextLine()
    while(dni!="END"){
        val employee = employees[dni]!!
        println("${employee.name} ${employee.surname} - ${employee.dni}, ${employee.address}")
        dni = scanner.nextLine()
    }
}

fun readEmployees(scanner: Scanner): Map<String, Employee> {
    val count = scanner.nextLine().toInt()
    val employees = mutableMapOf<String, Employee>()
    repeat(count){
        val employee : Employee = readEmployee(scanner)
        employees[employee.dni] = employee
    }
    return employees
}

fun readEmployeesVersion2(scanner: Scanner): Map<String, Employee> {
    val count = scanner.nextLine().toInt()
    val employeesList = List(count){ readEmployee(scanner) }
    return employeesList.associateBy { it.dni }
}

fun readEmployee(scanner: Scanner) : Employee {
    return Employee(scanner.nextLine(), scanner.nextLine(), scanner.nextLine(), scanner.nextLine())
}
