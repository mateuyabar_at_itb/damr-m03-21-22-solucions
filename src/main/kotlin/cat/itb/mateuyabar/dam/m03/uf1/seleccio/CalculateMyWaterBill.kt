package cat.itb.mateuyabar.dam.m03.uf1.seleccio

import java.util.*


fun fixedPrice(houseType: String): Double {
    return when(houseType) {
        "A" -> 2.50
        "B" -> 6.50
        "C" -> 7.36
        "D" -> 11.39
        "E" -> 12.29
        "F" -> 17.55
        "G" -> 28.45
        "H" -> 41.14
        "I" -> 62.27
        else -> 0.0
    }
}

fun pricePerLitter(watterAmmount: Double): Double {
    return when(watterAmmount.toInt()){
        in 0..6 -> 0.5941
        in 7..9 -> 1.1883
        in 10..15 -> 1.7824
        in 16..18 -> 2.3764
        else -> 2.92705
    }
}

fun main(){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val houseType = scanner.next()
    val watterAmmount = scanner.nextDouble()

    val fixedPrice  = fixedPrice(houseType)
    val pricePerLitter = pricePerLitter(watterAmmount)
    val price = pricePerLitter*watterAmmount + fixedPrice
    println(price)
}


