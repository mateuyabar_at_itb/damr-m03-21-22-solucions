package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val battleships = listOf(
        listOf(1, 1, 0, 0, 0, 0, 1),
        listOf(0, 0, 1, 0, 0, 0, 1),
        listOf(0, 0, 0, 0, 0, 0, 1),
        listOf(0, 1, 1, 1, 0, 0, 1),
        listOf(0, 0, 0, 0, 1, 0, 0),
        listOf(0, 0, 0, 0, 1, 0, 0),
        listOf(1, 0, 0, 0, 0, 0, 0))

    val x = scanner.nextInt()
    val y = scanner.nextInt()
    val message = if(battleships[x][y]==1){
        "tocat"
    } else {
        "aigua"
    }
    println(message)

}