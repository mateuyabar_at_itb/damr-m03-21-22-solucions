package cat.itb.mateuyabar.dam.m03.uf3

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import java.nio.file.Path
import java.util.*
import kotlin.io.path.exists
import kotlin.io.path.readText
import kotlin.io.path.writeText

class ShopItem(val quantity: Int, val name: String, val price: Double)

fun main() {
    val scanner = Scanner(System.`in`)
    val shopingList = readItemsFromFile()
    val shoItem = readShopItem(scanner)
    shopingList += shoItem
    print(shopingList)
    saveShopingList(shopingList)
}

val shopListPath = Path.of(System.getProperty("user.home"), "shoplist.json")

fun saveShopingList(shopingList: MutableList<ShopItem>) {
    val json = Json.encodeToString(shopingList)
    shopListPath.writeText(json)
}

fun readShopItem(scanner: Scanner) = ShopItem(scanner.nextInt(), scanner.next(), scanner.nextDouble())

fun readItemsFromFile() : MutableList<ShopItem> =
    if(shopListPath.exists())
        Json.decodeFromString<MutableList<ShopItem>>(shopListPath.readText())
    else
        mutableListOf()
