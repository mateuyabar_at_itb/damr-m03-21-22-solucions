package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val list = List(scanner.nextInt()){scanner.nextInt()}
    var sorted = true
    for(i in 0 until list.lastIndex){
        if(list[i]>list[i+1]){
            sorted = false
            break
        }
    }
    val result = if(sorted) "ordenats" else "desordenats"
    println(result)
}