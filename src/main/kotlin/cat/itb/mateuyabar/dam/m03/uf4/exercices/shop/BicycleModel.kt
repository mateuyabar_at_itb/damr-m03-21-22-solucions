package cat.itb.mateuyabar.dam.m03.uf4.exercices

import cat.itb.mateuyabar.dam.m03.uf4.exercices.shop.BicycleModel
import cat.itb.mateuyabar.dam.m03.uf4.exercices.shop.Brand
import cat.itb.mateuyabar.dam.m03.uf4.exercices.shop.VehicleModel


fun main() {
    val brand = Brand("Specialized", "USA")
    val bicycles = listOf(
        BicycleModel("Jett 24", brand, 5),
        BicycleModel("Hotwalk", brand, 7),
    )
    printVehicles(bicycles)
}

fun printVehicles(vehicles: List<VehicleModel>) {
    for(vehicle in vehicles)
        println(vehicle)
}
