package cat.itb.mateuyabar.dam.m03.uf1.data

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val originPrice = scanner.nextDouble()
    val discountPrice = scanner.nextDouble()
    val discount = originPrice - discountPrice
    val percentDiscount = discount / originPrice *100
    println(percentDiscount)
}