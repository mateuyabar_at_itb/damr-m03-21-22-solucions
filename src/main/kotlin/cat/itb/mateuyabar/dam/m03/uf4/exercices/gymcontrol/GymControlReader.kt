package cat.itb.mateuyabar.dam.m03.uf4.exercices.gymcontrol

interface GymControlReader {
    fun nextId() : String
}