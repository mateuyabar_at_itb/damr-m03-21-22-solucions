package cat.itb.mateuyabar.dam.m03.uf5.exercices.tads.sets

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val answer = scanner.nextLine()
    val answers = mutableSetOf<String>()
    while(answer!="END"){
        val added = answers.add(answer)
        if(!added)
            println("MEEEC!")
    }
}