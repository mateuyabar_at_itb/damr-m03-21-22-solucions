package cat.itb.mateuyabar.dam.m03.uf4.exercices.autonomouscar

interface CarSensors {
    fun isThereSomethingAt(direction: Direction) : Boolean
    fun go(direction : Direction)
    fun stop()
}

enum class Direction{
    FRONT, LEFT, RIGHT
}