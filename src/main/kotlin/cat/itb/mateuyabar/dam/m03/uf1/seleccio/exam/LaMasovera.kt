package cat.itb.mateuyabar.dam.m03.uf1.seleccio.exam

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val day = scanner.next()
    val result = when(day.lowercase()) {
        "dilluns" -> "Compra llums"
        "dimarts" -> "Compra naps"
        "dimecres" -> "Compra nespres"
        "dijous" -> "Compra nous"
        "divendres" -> "Faves tendres"
        "dissabte" -> "Tot s'ho gasta"
        "diumenge" -> "Tot s'ho menja"
        else -> ""
    }
    println(result)

}