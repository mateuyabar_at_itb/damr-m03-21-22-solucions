package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val padlock = MutableList(8){false}
    var buttonSelected = scanner.nextInt()
    while(buttonSelected!=-1){
        padlock[buttonSelected] = !padlock[buttonSelected]
        buttonSelected = scanner.nextInt()
    }
    println(padlock)
}