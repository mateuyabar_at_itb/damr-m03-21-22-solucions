package cat.itb.mateuyabar.dam.m03.uf1.data

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val rage1Start = scanner.nextInt()
    val rage1End = scanner.nextInt()
    val range2Start = scanner.nextInt()
    val range2End = scanner.nextInt()
    val valueToCheck = scanner.nextInt()

    val isInFirstRange = valueToCheck in rage1Start..rage1End
    val isInSecondRange = valueToCheck in range2Start..range2End
    val isInRange = isInFirstRange && isInSecondRange
    println(isInRange)
}