package cat.itb.mateuyabar.dam.m03.uf4.exam.gameenemies

import kotlin.math.max

sealed class Enemy(val name: String, var lives: Int) {
    val dead get() = lives==0

    fun attack(force: Int){
        if(dead) {
            println("L'enemic $name està mort")
        } else {
            attackWhenAlive(force)
            if(dead) {
                println("L'enemic $name està mort")
            } else {
                println("L'enemic $name té $lives punts de vida després d'un atac de força $force")
            }
        }
    }

    abstract fun attackWhenAlive(force: Int)

    fun substractLives(livesToSubsctract: Int){
        lives = max(0, lives-livesToSubsctract)
    }

    override fun toString() = "name: $name, live: $lives"

}

class Zombie(name: String, lives: Int, val sound: String):
    Enemy(name, lives){
    override fun attackWhenAlive(force: Int){
        println(sound)
        substractLives(force)
    }
}

class Troll(name: String, lives: Int, val resistance: Int):
    Enemy(name, lives){
    override fun attackWhenAlive(force: Int) {
        val realForce = max(0, force-resistance)
        substractLives(realForce)
    }
}

class Goblin(name: String, lives: Int):
    Enemy(name, lives){
    override fun attackWhenAlive(force: Int) {
        val realForce = when(force){
            in 0..4 -> 1
            else -> 5
        }
        substractLives(realForce)
    }
}
