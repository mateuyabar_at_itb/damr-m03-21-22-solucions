package cat.itb.mateuyabar.dam.m03.uf1.data

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val diameter = scanner.nextDouble()
    val radius = diameter / 2
    val area = Math.PI * radius * radius

    println(area)

}