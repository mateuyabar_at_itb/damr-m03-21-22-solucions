package cat.itb.mateuyabar.dam.m03.uf2.recursivity

import cat.itb.mateuyabar.dam.m03.uf2.functions.readIntList
import java.lang.Integer.max
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val list = readIntList(scanner)
    println(maxOfList(list.toMutableList()))
}

fun maxOfList(list: MutableList<Int>): Int {
    return if(list.size==1)
        list.first()
    else {
        val last = list.removeLast()
        max(last, maxOfList(list))
    }

}
