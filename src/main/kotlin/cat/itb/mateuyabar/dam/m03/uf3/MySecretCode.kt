package cat.itb.mateuyabar.dam.m03.uf3

import java.nio.file.Path
import java.util.*
import kotlin.io.path.writeText

fun main() {
    val scanner = Scanner(System.`in`)
    val text = scanner.nextLine()
    val path = Path.of("secret.txt")
    path.writeText(text)
}