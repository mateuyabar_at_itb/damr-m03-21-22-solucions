package cat.itb.mateuyabar.dam.m03.uf1.data

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val value : Double = scanner.nextDouble()
    val doubleValue = value*2
    println(doubleValue)
}