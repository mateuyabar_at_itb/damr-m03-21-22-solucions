package cat.itb.mateuyabar.dam.m03.uf1.nuls

data class Person(val name: String, val dni: String?)

fun main() {
    val person : Person? = Person("Ot", null)

    val dni = person?.dni?.uppercase()

    dni.let{ println(it)}

    if(dni!=null){
        println(dni)
    }



}