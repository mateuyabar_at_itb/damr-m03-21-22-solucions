package cat.itb.mateuyabar.dam.m03.uf1.generalexam

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val map = List(11){List(10) {scanner.next()} }
    for(i in 1 until map.lastIndex){
        for(j in 1 until map[0].lastIndex){
//            // option 1
//            var valid = true
//            for(i1 in -1..1){
//                for(j1 in -1..1){
//                    if(map[i][j]=="\uD83C\uDF33") //  🌳
//                        valid = false
//                }
//            }
//            if(valid) println("$j $i")

            // Option 2
            if(map[i-1][j-1]=="\uD83D\uDFE9" && map[i-1][j]=="\uD83D\uDFE9" && map[i-1][j+1]=="\uD83D\uDFE9"
                && map[i][j-1]=="\uD83D\uDFE9" && map[i][j]=="\uD83D\uDFE9" && map[i][j+1]=="\uD83D\uDFE9"
                && map[i+1][j-1]=="\uD83D\uDFE9" && map[i+1][j]=="\uD83D\uDFE9" && map[i+1][j+1]=="\uD83D\uDFE9"){
                println("$j $i")
            }
        }
    }
}