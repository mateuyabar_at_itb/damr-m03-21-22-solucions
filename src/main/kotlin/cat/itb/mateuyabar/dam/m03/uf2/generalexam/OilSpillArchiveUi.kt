package cat.itb.mateuyabar.dam.m03.uf2.generalexam

import java.util.*

fun main() {
    OilSpillArchiveUi().start()
}

class OilSpillArchiveUi(val scanner : Scanner = Scanner(System.`in`).useLocale(Locale.UK)) {
    val oilSpillArchive = OilSpillArchive()

    fun start(){
        val menu = """Chose option:
1. Add oil spill
2. List oil spills
3. Worst oil spill
4. Show Spill litters
5. Show spill gravity
6. Spills for company
7. Worst company
0. Exit"""
        println(menu)
        var action = scanner.nextLine().toInt()
        while(action!=0){
            processAction(action)
            println(menu)
            action = scanner.nextLine().toInt()
        }

    }

    private fun processAction(action: Int) {
        when(action){
            1-> addOilSpill()
            2 -> listOilSpills()
            3 -> worstOilSpill()
            4 -> showSpillLitters()
            5 -> showSpillGravity()
            6 -> spillsForCompany()
            7 -> worstCompany()
        }

    }

    private fun worstCompany() {
        val company = oilSpillArchive.worstCompany()
        println(company)
    }

    private fun spillsForCompany() {
        val companyName = scanner.nextLine()
        val oilSpills : List<OilSpill> = oilSpillArchive.findSpillsForCompany(companyName)
        printOilSpills(oilSpills)
    }

    private fun showSpillGravity() {
        val oilSpill = askUserForSpill()
        if(oilSpill!=null)
            println(oilSpill.gravity)
    }

    private fun showSpillLitters() {
        val oilSpill = askUserForSpill()
        if(oilSpill!=null)
            println(oilSpill.litters)
    }

    private fun askUserForSpill(): OilSpill? {
        val name = scanner.nextLine()
        val oilSpill = oilSpillArchive.findByName(name)
        if(oilSpill==null)
            println("Can't find oil spill")
        return oilSpill
    }

    private fun worstOilSpill() {
        val worstSpill = oilSpillArchive.worstSpill()
        printOilSpill(worstSpill)
    }

    private fun listOilSpills() {
        printOilSpills(oilSpillArchive.oilSpills)
    }

    private fun printOilSpills(oilSpills: List<OilSpill>) {
        println("name - company - litters - toxicity - gravity")
        for(oilSpill in oilSpills)
            printOilSpill(oilSpill)
    }

    private fun printOilSpill(oilSpill: OilSpill) {
        println("${oilSpill.name} - ${oilSpill.company} - ${oilSpill.litters} - ${oilSpill.toxicity} - ${oilSpill.gravity}")
    }

    private fun addOilSpill() {
        val name = scanner.nextLine()
        val company = scanner.nextLine()
        val litters = scanner.nextLine().toInt()
        val toxicity = scanner.nextDouble()
        scanner.nextLine()
        oilSpillArchive.create(name, company, litters, toxicity)
    }
}