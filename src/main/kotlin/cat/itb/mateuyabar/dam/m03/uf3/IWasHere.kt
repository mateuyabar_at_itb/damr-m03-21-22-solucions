package cat.itb.mateuyabar.dam.m03.uf3

import java.nio.file.OpenOption
import java.nio.file.Path
import java.nio.file.StandardOpenOption
import java.time.LocalDateTime
import java.util.*
import kotlin.io.path.writeText

fun main() {
    val home = System.getProperty("user.home")
    val path  = Path.of(home, "i_was_here.txt");
    path.writeText("I Was Here: ${LocalDateTime.now()}", options = arrayOf(StandardOpenOption.APPEND))
}