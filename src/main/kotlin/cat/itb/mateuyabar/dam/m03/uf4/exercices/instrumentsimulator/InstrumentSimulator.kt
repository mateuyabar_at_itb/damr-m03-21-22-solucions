package cat.itb.mateuyabar.dam.m03.uf4.exercices.instrumentsimulator

abstract class Instrument(){
    fun makeSounds(times: Int){
        repeat(times){
            makeSound()
        }
    }

    abstract fun makeSound()
}
class Triangle(val resonancia: Int): Instrument(){
    override fun makeSound() {
        println("T${vowels()}NC")
    }

    private fun vowels() = "I".repeat(resonancia)

}
class Drump(val sound: String) : Instrument(){
    override fun makeSound() {
        println("T${vowels()}M")
    }

    private fun vowels() = sound.repeat(3)

}

fun main() {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}