package cat.itb.mateuyabar.dam.m03.uf1.lists

fun main() {
    val map =listOf(listOf(1.5,1.6,1.8,1.7,1.6),listOf(1.5,2.6,2.8,2.7,1.6),listOf(1.5,4.6,4.4,4.9,1.6),listOf(2.5,1.6,3.8,7.7,3.6),listOf(1.5,2.6,3.8,2.7,1.6));
    var max = map[0][0]
    var maxI = 0
    var maxJ = 0
    for(i in map.indices){
        for(j in map.indices){
            val altitude = map[i][j]
            if(altitude>max){
                max = altitude
                maxI = i
                maxJ = j
            }
        }
    }
    println("$maxI, $maxJ: $max metres")
}