package cat.itb.mateuyabar.dam.m03.uf1.seleccio.exam

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val age = scanner.nextInt()
    val gender = scanner.nextInt()
    val castrated = scanner.nextInt()

    if (age < 2)
        println("vedell")
    else {
        if (gender == 2) {
            println("femella")
        } else {
            if (castrated == 2) {
                println("bou")
            } else {
                println("toro")
            }
        }
    }
}