package cat.itb.mateuyabar.dam.m03.uf2.recursivity

data class Person(val name: String, val father: Person? = null, val mother: Person? = null)

fun main() {
    val person = Person("Ot", Person("Jaume", Person("Pere"), Person("Mar")), Person("Maria", Person("John", null, Person("Fatima", Person("Ali")))))
    printFamilyTree(person)
}

fun printFamilyTree(person: Person?, tabs : Int = 0) {
    repeat(tabs){print("  ")}
    if(person==null)
        println("- ?")
    else {
        println(person.name)
        printFamilyTree(person.father, tabs+1)
        printFamilyTree(person.mother, tabs+1)
    }
}
