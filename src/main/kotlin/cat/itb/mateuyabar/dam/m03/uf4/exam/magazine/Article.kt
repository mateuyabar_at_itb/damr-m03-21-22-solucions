package cat.itb.mateuyabar.dam.m03.uf4.exam.magazine

import MagazinePrinter
import Printable

abstract class Article(val title: String, val author: String) : Printable {
    val fullTitle get() = "${articleType}: $title"
    abstract val articleType: String

    override fun print(magazinePrinter: MagazinePrinter){
        magazinePrinter.printTitle(fullTitle, author)
        printBody(magazinePrinter)
    }

    abstract fun printBody(magazinePrinter: MagazinePrinter)
}

class OpinionArticle(title: String, author: String, val text: String)
    : Article(title, author){
    override val articleType = "Opinió"

    override fun printBody(magazinePrinter: MagazinePrinter){
        magazinePrinter.printText(text)
    }
}

class PhotosArticle(title: String, author: String, val photos: List<String>)
    : Article(title, author){
    override val articleType = "Fotos"

    override fun printBody(magazinePrinter: MagazinePrinter){
        magazinePrinter.printPhotos(photos)
    }
}