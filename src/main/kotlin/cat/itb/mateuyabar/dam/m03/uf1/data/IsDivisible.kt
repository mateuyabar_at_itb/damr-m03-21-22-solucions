package cat.itb.mateuyabar.dam.m03.uf1.data

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val num1 = scanner.nextInt()
    val dibisibleBy = scanner.nextInt()
    val isDivisible = num1 % dibisibleBy == 0
    println(isDivisible)
}