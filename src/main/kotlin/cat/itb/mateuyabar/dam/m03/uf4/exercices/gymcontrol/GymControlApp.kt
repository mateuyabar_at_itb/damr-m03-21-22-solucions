package cat.itb.mateuyabar.dam.m03.uf4.exercices.gymcontrol

import java.util.*

fun main() {
    val users = mutableListOf<String>()
    val scanner = Scanner(System.`in`)
    val reader : GymControlReader = GymControlManualReader()
    while(true){
        readNewUser(users, reader)
    }
}

fun readNewUser(users: MutableList<String>, reader: GymControlReader) {
    val id = reader.nextId()
    val exists = users.remove(id)
    if(!exists)
        users.add(id)
    val action = if(exists) "Sortida" else "Entrada"
    println("$id $action")
}
