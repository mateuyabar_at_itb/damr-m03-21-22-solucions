package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val size = scanner.nextInt()
    val list = List(size) {scanner.nextInt()}
    val toFind = scanner.nextInt()

    var found = false
    for(value in list){
        if(value==toFind)
            found = true
        if(value>= toFind)
            break
    }
    println(found)
}