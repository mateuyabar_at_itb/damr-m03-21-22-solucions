package cat.itb.mateuyabar.dam.m03.uf1.iterative

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val multiplyNumber = scanner.nextInt()

    for(i in 1..9) {
        val result = multiplyNumber * i
        println("$i * $multiplyNumber = $result")
    }
}