package cat.itb.mateuyabar.dam.m03.uf1.data

import java.util.*

fun main(){
    // llegir enter de l'usuari
    val scanner = Scanner(System.`in`)
    val number = scanner.nextInt()
    // duplicar valor per 2
    val doubleNumber = number * 2
    // imprimir valor
    println(doubleNumber)
}