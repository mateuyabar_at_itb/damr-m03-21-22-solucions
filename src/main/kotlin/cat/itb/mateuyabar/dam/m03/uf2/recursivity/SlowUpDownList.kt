package cat.itb.mateuyabar.dam.m03.uf2.recursivity

import java.lang.Math.abs
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val list = MutableList<Int?>(scanner.nextInt()){null}
    var position = scanner.nextInt()
    while(position!=-1){
        val value = scanner.nextInt()
        list[position] = value
        position = scanner.nextInt()
    }

    val result = upDownlList(list)
    println(result)
}

fun upDownlList(list: List<Int?>, i : Int = 1) : List<Int?>? {
    if(i==list.size)
        return list
    if(list[i]==null){
        val upList = list.toMutableList()
        upList[i] = list[i-1]!!+1
        var validList = upDownlList(upList, i+1)
        if(validList!=null) return validList

        val downList = list.toMutableList()
        downList[i] = list[i-1]!!-1
        return upDownlList(downList, i+1)
    } else {
        val diff = abs(list[i]!!-list[i-1]!!)
        return if(diff==1)
            upDownlList(list, i+1)
        else
            null
    }

}
