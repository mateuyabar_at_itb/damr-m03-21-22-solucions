package cat.itb.mateuyabar.dam.m03.uf1.iterative

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val rows = scanner.nextInt()

    for(row in 1..rows) {
        repeat(row) {
            print("# ")
        }
        println()
    }


    for(apples in 1..10){
        println("$apples apples")
    }

}