package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val daysOfWeek = listOf("dilluns",
        "dimarts", "dimecres", "dijous", "divendres",
        "dissabte", "diumenge")
    val dayNumber = scanner.nextInt()
    val dayOfWeek = daysOfWeek[dayNumber]
    println(dayOfWeek)
}