package cat.itb.mateuyabar.dam.m03.uf2.modularitat

data class Cell(
    val i: Int,
    val j:Int,
    var hasBomb: Boolean = false,
    var opened: Boolean = false,
    var adjacentBombs : Int = -1
) {
    fun showInformation():String {
        TODO()
    }
}