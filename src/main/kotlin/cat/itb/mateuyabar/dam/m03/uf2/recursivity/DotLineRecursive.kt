package cat.itb.mateuyabar.dam.m03.uf2.recursivity

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val count = scanner.nextInt()
    println(points(count))
}

fun points(count: Int): String {
    return if(count==0)
        ""
    else
        points(count-1)+"."
}
