package cat.itb.mateuyabar.dam.m03.uf1.introfunctions

import java.util.*
import kotlin.math.sqrt

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val height = scanner.nextDouble()
    val weight = scanner.nextDouble()
    val diagonal = sqrt(height*height+weight*weight)
    println(diagonal)
}