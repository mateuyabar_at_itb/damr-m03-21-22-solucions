package cat.itb.mateuyabar.dam.m03.uf1.nuls

import cat.itb.mateuyabar.dam.m03.uf1.lists.readIntList
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val list = readIntList(scanner)
    val toSearch = scanner.nextInt()

    var foundPosition : Int? = null
    for(i in list.indices){
        if(list[i]==toSearch){
            foundPosition = i
            break
        }
    }
    println(foundPosition)
}