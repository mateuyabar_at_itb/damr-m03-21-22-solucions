package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val list = mutableListOf<Int>()

    var value = scanner.nextInt()
    while(value!=-1) {
        var found = false
        for (item in list) {
            if (item == value) {
                found = true
                break
            }
        }
//      val found = value in list
        if (!found)
            list += value
        value = scanner.nextInt()
    }
    println(list)
}