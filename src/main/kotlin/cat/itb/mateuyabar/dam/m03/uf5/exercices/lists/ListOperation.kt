package cat.itb.mateuyabar.dam.m03.uf5.exercices.lists

import cat.itb.mateuyabar.dam.m03.uf1.lists.readIntList
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val list = List(scanner.nextInt()) { scanner.nextInt()}
    list.filter { it%10!=3 }
        .sortedDescending()
        .forEach{
            println(it)
        }
}