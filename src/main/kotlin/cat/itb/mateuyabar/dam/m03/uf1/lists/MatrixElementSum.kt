package cat.itb.mateuyabar.dam.m03.uf1.lists

fun sumMatrix(matrix : List<List<Int>>) : Int {
    var sum = 0
    for(row in matrix){
        for(value in row){
            sum+=value
        }
    }
    return sum
}

fun main() {
    val matrix = listOf(listOf(2,5,1,6),listOf(23,52,14,36),listOf(23,75,81,64))

    val sum = sumMatrix(matrix)

    println(sum)
}