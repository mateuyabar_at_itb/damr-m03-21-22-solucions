package cat.itb.mateuyabar.dam.m03.uf3

import java.nio.file.Path
import java.util.*

fun main() {
    val speeds = readSpeeds()
    val max = speeds.maxOrNull()
    val min = speeds.minOrNull()
    val avg = speeds.average()
    println("Velocitat màxima: ${max}km/h")
    println("Velocitat mínima: ${min}km/h")
    println("Velocitat mitjana: ${avg}km/h")

}

fun readSpeeds(): List<Int> {
    val scanner = Scanner(System.`in`)
    val path = Path.of(scanner.nextLine())
    val fileScanner = Scanner(path)
    val speeds = mutableListOf<Int>()
    while(fileScanner.hasNext()){
        speeds+=fileScanner.nextInt()
    }
    return speeds
}
