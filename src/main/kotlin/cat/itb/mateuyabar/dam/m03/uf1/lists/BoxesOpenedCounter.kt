package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val boxes = MutableList(11){ 0 }

    var box = scanner.nextInt()
    while(box!=-1) {
        boxes[box]++
        box = scanner.nextInt()
    }
    println(boxes)
}