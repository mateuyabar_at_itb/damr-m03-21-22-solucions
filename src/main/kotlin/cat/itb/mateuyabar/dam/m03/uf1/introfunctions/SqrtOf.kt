package cat.itb.mateuyabar.dam.m03.uf1.introfunctions

import java.lang.Math.sqrt
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val value = scanner.nextDouble()
    val result = sqrt(value)
    println(result)
}