package cat.itb.mateuyabar.dam.m03.uf4.exercices.shop

import cat.itb.mateuyabar.dam.m03.uf4.exercices.printVehicles

fun main() {
    val brand = Brand("Specialized", "USA")
    val vehicle : List<VehicleModel> = listOf(
        BicycleModel("Jett 24", brand, 5),
        ScooterModel("Hotwalk", brand, 45.3),
    )
    printVehicles(vehicle)
}