package cat.itb.mateuyabar.dam.m03.uf1.lists

fun main() {
    val list = MutableList(50){ 0f }
    list[0] = 31f
    list[1] = 56f
    list[7] = 12f
    list[list.lastIndex] = 79f
    println(list)
}