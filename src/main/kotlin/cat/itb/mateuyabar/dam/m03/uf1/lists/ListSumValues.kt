package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val list = readIntList(scanner)
    var sum = 0
    for(value in list){
        sum += value
    }
    println(sum)
    // list.sum()
}