package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {


    val scanner = Scanner(System.`in`)
    val list1 = readIntList(scanner)
    val list2 = readIntList(scanner)
    var equal = true
    if(list1.size!=list2.size) {
        equal = false
    } else {
        for(i in 0..list1.lastIndex){
            if(list1[i]!=list2[i]){
                equal = false
                break
            }
        }
    }
    val text = if(equal) "són iguals" else "no són iguals"
    println(text)

}