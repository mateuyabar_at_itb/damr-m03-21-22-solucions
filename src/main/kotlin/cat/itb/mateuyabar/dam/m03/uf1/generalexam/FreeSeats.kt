package cat.itb.mateuyabar.dam.m03.uf1.generalexam

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val spaces = scanner.nextInt()
    val seats = scanner.nextInt()
    var found = false
    var currentEmpty = 0
    for(i in 1..seats){
        val available = scanner.nextInt()==0
        if(available) {
            currentEmpty++
            if(currentEmpty>=spaces){
                found = true
                break
            }
        } else {
            currentEmpty = 0
        }
    }
    val message = if(found)
        "Hi ha $spaces seients consecutius"
    else
        "No hi ha $spaces seients consecutius"
    println(message)
}