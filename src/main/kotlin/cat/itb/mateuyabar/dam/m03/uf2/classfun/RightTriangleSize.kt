package cat.itb.mateuyabar.dam.m03.uf2.classfun

import java.lang.Math.sqrt
import java.util.*

data class RightTriangle(val width: Double, val height: Double){
    val area get() = width*height/2
    val perimeter get() = width+height+hypothen
    val hypothen get() = sqrt(width*width+height*height)

    fun toOutput() = "Un triangle de $width x $height té $area d'area i $perimeter de perímetre."
}

fun main() {
    val scanner = Scanner(System.`in`)
    val triangles = readTriangles(scanner)
    printTriangles(triangles)
}

fun printTriangles(triangles: List<RightTriangle>) {
    for(triangle in triangles){
        println(triangle.toOutput())
    }
}

fun readTriangles(scanner: Scanner): List<RightTriangle> {
    val count = scanner.nextInt()
    return List(count){
        readTriangle(scanner)
    }
}

fun readTriangle(scanner: Scanner):RightTriangle {
    val width = scanner.nextDouble()
    val height = scanner.nextDouble()
    return RightTriangle(width, height)
}
