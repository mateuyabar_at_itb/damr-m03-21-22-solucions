package cat.itb.mateuyabar.dam.m03.uf2.classfun

import java.awt.Robot

class BasicRobot(val x : Double=0.0, val y : Double=0.0, val speed: Double=1.0) {
    fun moveUp(){
        //y += speed
    }
    fun moveLeft(){
        TODO()
    }
    fun accelerate(){
        TODO()
    }
    fun toStringPosition() = "La posició del robot és (0.0, 0.0)"

}

fun main() {
    val robot = BasicRobot()
    println(robot.toStringPosition())

}