package cat.itb.mateuyabar.dam.m03.uf1.iterative

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    var number = scanner.nextInt()
    var sum = 0

    while(number>0) {
        val lastDigit = number % 10
        sum += lastDigit
        number /= 10
    }
    println(sum)
}