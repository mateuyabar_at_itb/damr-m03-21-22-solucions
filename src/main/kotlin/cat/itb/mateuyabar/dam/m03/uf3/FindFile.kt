package cat.itb.mateuyabar.dam.m03.uf3

import java.nio.file.Path
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)

    val path = Path.of(scanner.nextLine())
    val toFind = scanner.nextLine()
    path.toFile().walk().forEach { file ->
        if(file.toPath().fileName.endsWith(toFind))
            println(path)
    }
}