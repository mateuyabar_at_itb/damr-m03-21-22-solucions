package cat.itb.mateuyabar.dam.m03.uf2.mines

import java.util.*

data class Cell(val x: Int, val y:Int, var bomb: Boolean=false, var opened: Boolean=true, var nextCount:Int = -1){
    val bombAsInt get() = if(bomb) 1 else 0
    fun showInformation():String {
      return if(!opened){
          "X"
      } else if(bomb){
          "B"
      } else {
          nextCount.toString()
      }
    }

    fun open() {
        opened = true
    }

}

data class Board(val matrix: List<List<Cell>>, val bombs:Int){
    val size get() = matrix.size
    constructor(bombs: Int, size: Int) : this(List(size){i-> List(size){j-> Cell(i,j)} }, bombs)

    fun generateBombsList(){
        var bombsCount = 0
        while(bombsCount<bombs) {
            val x = (0..size - 1).random()
            val y = (0..size - 1).random()
            if (!matrix[x][y].bomb) {
                addBomb(x, y)
                bombsCount++
            }
        }
    }

    fun calculateAdjacencies(){
        for(row in matrix){
            for(cell in row){
                calculateAdjacencies(cell.x, cell.y)
            }
        }
    }

    fun calculateAdjacencies(x: Int, y:Int){
        var count = 0
        for(i in x-1..x+1){
            for(j in y-1..y+1){
                if(i in matrix.indices && j in matrix[i].indices && !(x==i && y==j)){
                    count+= matrix[i][j].bombAsInt
                }
            }
        }
        matrix[x][y].nextCount = count
    }

    private fun addBomb(x: Int, y: Int) {
        matrix[x][y].bomb = true
    }

    fun toOutput():String{
        var output = ""
        for(row in matrix){
            for(cell in row){
                output+= cell.showInformation()
            }
            output+="\n"
        }
        return output
    }

    ///-----------ADDED
    fun open(x: Int, y:Int) : Boolean{
        matrix[x][y].open()
        return matrix[x][y].bomb
    }

    fun openAll(){
        for(row in matrix) {
            for (cell in row) {
                cell.open()
            }
        }
    }
}

data class Game(val size: Int, val bombs: Int){
    val board : Board
    init{
        board = Board(size, bombs)
        board.generateBombsList()
        board.calculateAdjacencies()
    }

}

data class UI(val game: Game){
    val scanner = Scanner(System.`in`)

    fun play(){
        while(true){
            println(game.board.toOutput())
            val x = scanner.nextInt()
            val y = scanner.nextInt()
            val bomb = game.board.open(x,y)
            if(bomb){
                println("Bomb!")
                break
            }
        }
        game.board.openAll()
        println(game.board.toOutput())
    }

}

fun main() {
    val game = Game(5,5)
    val ui = UI(game)
    ui.play()
}