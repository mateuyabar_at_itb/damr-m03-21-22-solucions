package cat.itb.mateuyabar.dam.m03.uf1.generalexam

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val count = scanner.nextInt()
    scanner.nextLine()
    val lines = List(count){
        scanner.nextLine()
    }
    var capitalRequired = false
    for(i in lines.indices){
        val line = lines[i]
        if(capitalRequired && !line[0].isUpperCase()){
            println(i+1)
        }
        capitalRequired = line[line.lastIndex] == '.'
    }
}