package cat.itb.mateuyabar.dam.m03.uf2.classfun

import java.util.*

data class Client(val name : String, val amount : Int)

data class CampSite(val clients : MutableList<Client> = mutableListOf()) {
    val slots get() = clients.size
    val personAmount : Int get() {
        var total = 0
        for(client in clients)
            total += client.amount
        return total
    }

    fun checkIn(name: String, ammount: Int) {
        clients += Client(name, ammount)
    }

    fun checkOut(name: String) {
        val position = findClient(name)
        clients.removeAt(position!!)
    }

    private fun findClient(name: String): Int? {
        for(i in clients.indices){
            if(clients[i].name==name)
                return i
        }
        return null
    }
}

fun main() {
    campSiteOrzanizerMain()
}

fun campSiteOrzanizerMain() {
    val scanner = Scanner(System.`in`)
    val campSite = CampSite()
    var operation = scanner.next()
    while(operation!="END"){
        when(operation){
            "ENTRA" -> clientCheckIn(scanner, campSite)
            "MARXA" -> clientCheckOut(scanner, campSite)
        }
        printCurrentData(campSite)
        operation = scanner.next()
    }
}


fun printCurrentData(campSite: CampSite) {
    println("parcel·les: ${campSite.slots}")
    println("persones: ${campSite.personAmount}")

}


fun clientCheckOut(scanner: Scanner, campSite: CampSite) {
    val name = scanner.next()
    campSite.checkOut(name)
}

fun clientCheckIn(scanner: Scanner, campSite: CampSite) {
    val ammount = scanner.nextInt()
    val name = scanner.next()
    campSite.checkIn(name, ammount)
}
