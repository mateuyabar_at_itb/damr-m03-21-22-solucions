package cat.itb.mateuyabar.dam.m03.uf4.exercices.figures

fun main() {
    RectangleFigure(RED, 5, 4).paint()
    println()
    LeftPiramidFigure(YELLOW, 3).paint()
    println()
    RectangleFigure(GREEN, 5, 3).paint()
}
