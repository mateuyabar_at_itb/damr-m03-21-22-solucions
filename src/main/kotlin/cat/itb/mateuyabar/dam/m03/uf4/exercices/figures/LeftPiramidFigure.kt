package cat.itb.mateuyabar.dam.m03.uf4.exercices.figures

class LeftPiramidFigure(color: String, val base: Int)
    : Figure(color){


    override fun paintDots() {
        repeat(base){
            repeat(it+1){
                print("X")
            }
            println()
        }
    }
}