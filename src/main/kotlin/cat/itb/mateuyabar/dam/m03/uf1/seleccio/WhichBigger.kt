package cat.itb.mateuyabar.dam.m03.uf1.seleccio

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val value1 = scanner.nextInt()
    val value2 = scanner.nextInt()
    val result = if(value1>value2)
        value1
    else
        value2
    println(result)

}