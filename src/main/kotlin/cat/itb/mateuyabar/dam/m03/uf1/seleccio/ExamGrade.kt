package cat.itb.mateuyabar.dam.m03.uf1.seleccio

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val grade = scanner.nextDouble()

    if(grade<0 && grade>10){
        println("Nota Invàlida")
    } else if(grade<5){
        println("Suspès")
    } else if(grade<6){
        println("Suicient")
    } else if(grade<9){
        println("Notable")
    } else{
        println("Excel·lent")
    }

//    if(grade in 9.0..10.0){
//
//    } else if(grade in 7.0..9.0){
//
//    }
}