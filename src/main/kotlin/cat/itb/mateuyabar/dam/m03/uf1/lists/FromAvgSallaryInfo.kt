package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

data class Employee(val name: String, val salary: Int)

fun main() {
    val scanner = Scanner(System.`in`)
    val employees = List(scanner.nextInt()){
        val name = scanner.next()
        val salary = scanner.nextInt()
        Employee(name, salary)
    }

    var sumSalary = 0
    for(employee in employees){
        sumSalary += employee.salary
    }
    val averageSalary = sumSalary/employees.size

    for(employee in employees){
        if(employee.salary<averageSalary){
            println(employee.name)
        }
    }
}