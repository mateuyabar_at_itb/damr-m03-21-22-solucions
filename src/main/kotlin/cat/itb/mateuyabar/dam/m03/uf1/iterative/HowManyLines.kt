package cat.itb.mateuyabar.dam.m03.uf1.iterative

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    var lineCounter = 0
    var line = scanner.nextLine()
    while(line!="END"){
        lineCounter++
        line = scanner.nextLine()
    }
    println(lineCounter)
}