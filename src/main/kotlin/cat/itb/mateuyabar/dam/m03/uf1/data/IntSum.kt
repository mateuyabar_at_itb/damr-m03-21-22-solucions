package cat.itb.mateuyabar.dam.m03.uf1.data

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    // llegir un enter
    val value = scanner.nextInt()
    // llegir un altre enter
    val otherValue = scanner.nextInt()
    // sumar els dos enters
    val sumOfValues = value+otherValue
    // imprimir
    println(sumOfValues)
}