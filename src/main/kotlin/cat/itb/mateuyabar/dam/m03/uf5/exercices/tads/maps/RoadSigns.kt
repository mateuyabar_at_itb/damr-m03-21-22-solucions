package cat.itb.mateuyabar.dam.m03.uf5.exercices.tads.maps

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val signs = readSigns(scanner)
    processQuestions(scanner, signs)

}

fun processQuestions(scanner: Scanner, signs: Map<Int, String>) {
    val count = scanner.nextInt()
    repeat(count){
        val km = scanner.nextInt()
        val sign = signs[km] ?: "no hi ha cartell"
        println(sign)
    }

}

fun readSigns(scanner: Scanner): Map<Int, String> {
    val signs =mutableMapOf<Int, String>()
    val count = scanner.nextInt()
    repeat(count){
        val km = scanner.nextInt()
        val text = scanner.nextLine()
        signs[km] = text
    }
    return signs

}
