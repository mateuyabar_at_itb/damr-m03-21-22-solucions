package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val list = readIntList(scanner)
    var capicua = true
    for(i in 0..(list.size/2)){
        if(list[i]!=list[list.lastIndex-1]) {
            capicua = false
            break
        }
    }
    if(capicua) println("cap i cua")

//    if(list.reversed() == list)
//        println("cap i cua")
}