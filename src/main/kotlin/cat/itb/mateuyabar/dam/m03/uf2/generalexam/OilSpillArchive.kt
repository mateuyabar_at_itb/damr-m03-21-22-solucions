package cat.itb.mateuyabar.dam.m03.uf2.generalexam

class OilSpillArchive(val oilSpills: MutableList<OilSpill> = mutableListOf()) {
    fun create(name: String, company: String, litters: Int, toxicity: Double) {
        val oilSpill = OilSpill(name, company, litters, toxicity)
        oilSpills+=oilSpill
    }

    fun worstSpill(): OilSpill {
        var result = oilSpills.first()
        for(oilSpill in oilSpills){
            if(oilSpill.gravity>result.gravity)
                result = oilSpill
        }
        return result
    }

    fun findByName(name: String): OilSpill? {
        for(oilSpill in oilSpills){
            if(oilSpill.name==name)
                return oilSpill
        }
        return null
    }

    fun findSpillsForCompany(companyName: String): List<OilSpill> {
        val list = mutableListOf<OilSpill>()
        for(oilSpill in oilSpills){
            if(oilSpill.company == companyName)
                list+=oilSpill
        }
        return list
    }

    fun worstCompany(): String {
        val companies : List<String> = listCompanies()
        var worst = companies.first()
        for(company in companies){
            if(totalGravity(company)>totalGravity(worst)){
                worst = company
            }
        }
        return worst
    }

    private fun totalGravity(company: String): Double {
        val companySpills = findSpillsForCompany(company)
        var sum = 0.0
        for(oilSpill in companySpills){
            sum += oilSpill.gravity
        }
        return sum
    }

    private fun listCompanies(): List<String> {
        val companies = mutableListOf<String>()
        for(oilSpill in oilSpills){
            if(oilSpill.company !in companies)
                companies+=oilSpill.company
        }
        return companies
    }
}