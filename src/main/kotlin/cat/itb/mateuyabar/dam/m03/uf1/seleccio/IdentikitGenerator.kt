package cat.itb.mateuyabar.dam.m03.uf1.seleccio

import java.util.*

fun getHairDraw(hairName: String):String {
    return when(hairName){
        "arrissats"-> "@@@@@"
        "llisos" -> "VVVVV"
        else -> "XXXXX"
    }
}

fun main(){
    val scanner = Scanner(System.`in`)
    val hair = scanner.next()
    val eyes = scanner.next()
    val nose = scanner.next()
    val mouth = scanner.next()
    val hairDraw = getHairDraw(hair)
    val eyesDraw = when(eyes){
        "aclucats"-> ".-.-."
        "rodons" -> ".o-o."
        else -> ".*-*."
    }
    val noseDraw = when(nose){
        "aixafat" -> "..0.."
        "arromangat" -> "..C.."
        else -> "..V.."
    }
    val mouthDraw = when(mouth){
        "aixafat" -> "..0.."
        "arromangat" -> "..C.."
        else -> "..V.."
    }

    println(hairDraw)
    println(eyesDraw)
    println(noseDraw)
    println(mouthDraw)
}