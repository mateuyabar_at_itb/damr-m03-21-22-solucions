package cat.itb.mateuyabar.dam.m03.uf2.functions

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val list = readIntList(scanner)
    val minPrice = min(list)
    println("El producte més econòmic val: $minPrice€")
}