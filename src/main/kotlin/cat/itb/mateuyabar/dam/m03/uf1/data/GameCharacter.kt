package cat.itb.mateuyabar.dam.m03.uf1.data

class Position(var x: Double, var y: Double)

class Robot(val color: String,
            var life: Int,
            var position: Position,
            var angle: Double)