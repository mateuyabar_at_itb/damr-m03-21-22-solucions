package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val students = mutableListOf("Magalí", "Magdalena", "Magí","Màlika", "Manel", "Manela", "Mar", "Marc", "Margalida", "Marçal", "Marcel", "Maria", "Maricel", "Marina", "Marta", "Martí", "Martina", "Mausi", "Mei-Xiu", "Miquel", "Ming", "Mohamed")

    var studentNumber = scanner.nextInt()
    while(studentNumber!=-1){
        students.removeAt(studentNumber)
        studentNumber = scanner.nextInt()
    }
    println(students)
}