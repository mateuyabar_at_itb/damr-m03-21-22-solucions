package cat.itb.mateuyabar.dam.m03.uf5.exercices.lists

import java.util.*

data class Country(
    val name: String,
    val capital : String,
    val area: Int,
    val density: Int
) {
    val population get() = area * density
}

fun main() {
    val scanner = Scanner(System.`in`)
    val coutries = readCountries(scanner)

    coutries.filter { it.area > 1000 && it.density > 5 }
        .sortedBy { it.capital }
        .forEach{   println(it) }
}

fun readCountries(scanner: Scanner): List<Country> {
    return List(scanner.nextLine().toInt()){
        readCountry(scanner)
    }

}

fun readCountry(scanner: Scanner):Country {
    return Country(scanner.next(), scanner.next(),
        scanner.nextInt(), scanner.nextInt())
}
