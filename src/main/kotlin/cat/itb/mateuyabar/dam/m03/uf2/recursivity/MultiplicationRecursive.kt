package cat.itb.mateuyabar.dam.m03.uf2.recursivity

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val value1 = scanner.nextInt()
    val value2 = scanner.nextInt()
    println(multiplyValues(value1, value2))
}

fun multiplyValues(v1: Int, times: Int): Int {
    return if(times==0)
        0
    else
        v1+multiplyValues(v1, times-1)
}
