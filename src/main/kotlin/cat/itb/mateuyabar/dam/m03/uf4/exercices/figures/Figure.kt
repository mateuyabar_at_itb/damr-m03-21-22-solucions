package cat.itb.mateuyabar.dam.m03.uf4.exercices.figures

abstract class Figure(val color: String) {
    fun paint(){
        configureConsoleColor()
        paintDots()
        resetConsoleColor()
    }

    abstract fun paintDots()

    fun resetConsoleColor() {
        print(RESET)
    }

    fun configureConsoleColor() {
        print(color)
    }
}