package cat.itb.mateuyabar.dam.m03.uf1.introfunctions

import java.util.*

class Employee(val name: String,
               val surname : String,
               val officeNumber : Int)

fun printEmployee(employee : Employee){
    println("Empleada: ${employee.name} ${employee.surname} - Despatx: ${employee.officeNumber}")
}

fun readEmployee(scanner : Scanner) : Employee {
    val name = scanner.nextLine()
    val surname = scanner.nextLine()
    val officeNumber = scanner.nextInt()
    return Employee(name, surname, officeNumber)
}

fun main() {
    val scanner = Scanner(System.`in`)
    val employee = readEmployee(scanner)
    printEmployee(employee)
}