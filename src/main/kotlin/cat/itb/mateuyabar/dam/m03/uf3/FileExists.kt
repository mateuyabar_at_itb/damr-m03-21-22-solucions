package cat.itb.mateuyabar.dam.m03.uf3

import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.exists

fun main() {
    val scanner = Scanner(System.`in`)
    val pathString = scanner.nextLine()
    val path = Path(pathString)
    println(path.exists())
}