package cat.itb.mateuyabar.dam.m03.uf1.seleccio

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val personCount = scanner.nextInt()
    val cookiesCount = scanner.nextInt()
    if(cookiesCount % personCount == 0)
        println("Let's Eat!")
    else
        println("Let's Fight")
}