package cat.itb.mateuyabar.dam.m03.uf2.recursivity

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val num = scanner.nextInt()
    println(factorial(num))
}

fun factorial(num: Int): Int {
    if(num==0)
        return 1
    else
        return num * factorial(num-1)
}
