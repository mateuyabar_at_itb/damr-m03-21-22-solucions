package cat.itb.mateuyabar.dam.m03.uf1.data

import java.util.*

class Employee(val name: String,
               val surname : String,
               val officeNumber : Int)

fun main() {
    val scanner = Scanner(System.`in`)
    val name = scanner.nextLine()
    val surname = scanner.nextLine()
    val officeNumber = scanner.nextInt()
    val employee = Employee(name, surname, officeNumber)

    println("Empleada: ${employee.name} ${employee.surname} - Despatx: ${employee.officeNumber}")
}