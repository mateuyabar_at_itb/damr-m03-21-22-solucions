package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

data class Country(val name: String, val population: Int)

fun main() {
    val scanner = Scanner(System.`in`)
    val size = scanner.nextInt()
    scanner.nextLine()
    val countries = List(size){
        val name = scanner.nextLine()
        val population = scanner.nextInt()
        scanner.nextLine()
        Country(name, population)
    }
    val minPopulation = scanner.nextInt()

    for(country in countries){
        if(country.population>minPopulation){
            println(country.name)
        }
    }


}