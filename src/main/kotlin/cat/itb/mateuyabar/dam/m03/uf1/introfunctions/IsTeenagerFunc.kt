package cat.itb.mateuyabar.dam.m03.uf1.introfunctions

import java.util.*

fun isATeenager(age:Int) = age in 10..20

fun main(){
    val scanner = Scanner(System.`in`)
    val age = scanner.nextInt()
    val isATeenager = isATeenager(age)
    println(isATeenager)
}