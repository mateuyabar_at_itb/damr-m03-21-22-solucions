package cat.itb.mateuyabar.dam.m03.uf4.exercices.shop


data class Brand(val name: String, val country: String)

open  class VehicleModel(val name: String, val brand: Brand)
class BicycleModel(name: String, brand: Brand, val gears: Int)
    : VehicleModel(name, brand){
    override fun toString() =
        "BicycleModel{name='$name', gears=$gears, brand=$brand}"
}
class ScooterModel(name: String, brand: Brand, val power: Double)
    : VehicleModel(name, brand) {
    override fun toString() = "ScooterModel{name='$name', power=$power, brand=$brand}"
    }
