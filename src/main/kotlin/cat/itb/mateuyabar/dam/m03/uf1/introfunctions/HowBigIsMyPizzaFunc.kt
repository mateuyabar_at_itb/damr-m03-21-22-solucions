package cat.itb.mateuyabar.dam.m03.uf1.introfunctions

import java.util.*

fun areaOfCircle(radius: Double) = Math.PI * radius *radius

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val diameter = scanner.nextDouble()
    val area = areaOfCircle(diameter/2)
    println(area)
}