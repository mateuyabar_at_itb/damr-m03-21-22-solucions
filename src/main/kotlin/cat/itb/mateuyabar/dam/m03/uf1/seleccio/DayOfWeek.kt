package cat.itb.mateuyabar.dam.m03.uf1.seleccio

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val dayNumber = scanner.nextInt()
    val dayName = when(dayNumber){
        1->"dilluns"
        2->"dimarts"
        3->"dimecres"
        4->"dijous"
        5->"divendres"
        6->"dissabte"
        7->"diumenge"
        else ->"error"
    }
    println(dayName)
}