package cat.itb.mateuyabar.dam.m03.uf5.exercices.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val countries = readCountries(scanner)
    val result = countries.filter { it.area < 1200000 }
        .map{ it.population }
        .average()
    println(result)

}