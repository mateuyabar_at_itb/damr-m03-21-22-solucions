package cat.itb.mateuyabar.dam.m03.uf1.data.project

import java.time.Year
import java.util.*

val scanner = Scanner(System.`in`).useLocale(Locale.UK)
fun main() {
    welcome("TrainingAssistant") // change it as you need
    ageCalculator()
    imcCalculator()
    trainingSchedule()
}

fun welcome(assistantName: String) {
    println("Hello! My name is $assistantName.")
    println("Please, tell me your name.")
    val name = scanner.nextLine()
    println("What a great name you have, $name!")
}
fun ageCalculator() {
    println("Please, tell me which year you were born.")
    val yearBorn = scanner.nextInt()
    val currentYear = Year.now().value
    val ageMax = currentYear-yearBorn
    val ageMin = ageMax -1
    println("You are between $ageMin - $ageMax years old. That's a good age for practicing sport.")
}

fun imcCalculator() {
    println("Let's check some of your parameters")
    println("Tell me your weight in kg")
    val weight = scanner.nextDouble()
    println("Tell me your height in m")
    val height = scanner.nextDouble()
    val imc = weight/(height*height)
    println("Your IMC is $imc")

    val insuficentWeight = imc < 18.5
    val overWeight = imc in 25.0..50.0
    val obesity = imc > 50.0
    val normal = imc>=18.5 && imc <25.0
//    val normal = !insuficentWeight && !overWeight && !obesity
//    val normal = !(insuficentWeight || overWeight || obesity)
    println("Checking insufficient weight.... $insuficentWeight")
    println("Checking normal weight.... $normal")
    println("Checking overweight.... $overWeight")
    println("Checking obesity.... $obesity")

}
fun trainingSchedule() {
    println("I'll tell you your training plan.")
    println("How many hours would you like to train?")
    val hours = scanner.nextInt()
    println("How many days can you train?")
    val days = scanner.nextInt()
    val minHoursPerDay = hours/days
    val maxHoursPerDay = minHoursPerDay+1
    val daysMaxHours = hours%days
    val daysMinHours = days-daysMaxHours
    println("Your routine sport could be:")
    println("$daysMaxHours days $maxHoursPerDay hours")
    println("$daysMinHours days $minHoursPerDay hours")
}