package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val list = mutableListOf<Int>()
    var number = scanner.nextInt()
    while(number!=-1) {
        list.add(0, number)
        number = scanner.nextInt()
    }
    println(list)
}