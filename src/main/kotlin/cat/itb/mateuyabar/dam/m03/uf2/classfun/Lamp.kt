package cat.itb.mateuyabar.dam.m03.uf2.classfun

import cat.itb.mateuyabar.dam.m03.uf1.data.project.scanner
import java.util.*

data class Lamp(var turnedOn:Boolean = false){
    fun turnOn(){ turnedOn = true}
    fun turnOff(){ turnedOn = false}
    fun toggle(){ turnedOn = !turnedOn }
    fun perform(action : String){
        when(action){
            "TURN ON" -> turnOn()
            "TURN OFF" -> turnOff()
            "TOGGLE" -> toggle()
        }
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    val lamp = Lamp()
    var action = scanner.nextLine()
    while(action!="END"){
        lamp.perform(action)
        println(lamp.turnedOn)
        action = scanner.nextLine()
    }

}