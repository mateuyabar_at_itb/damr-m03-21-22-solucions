package cat.itb.mateuyabar.dam.m03.uf1.iterative

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val rangeStart = scanner.nextInt()
    val rangeEnd = scanner.nextInt()
    for(i in rangeStart+1 until rangeEnd)
        print(i)

}