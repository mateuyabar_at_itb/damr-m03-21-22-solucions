package cat.itb.mateuyabar.dam.m03.uf1.seleccio

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val value = scanner.nextInt()
    val text = when(value){
        1 -> "Un"
        2 -> "Dos"
        3,4,5-> "Forces"
        else -> "molts"
    }
    println(text)
}