package cat.itb.mateuyabar.dam.m03.uf1.data

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    var temperature = scanner.nextDouble()
    val increase = scanner.nextDouble()
    temperature += increase
    println("La temperatura actual és ${temperature}º")
}