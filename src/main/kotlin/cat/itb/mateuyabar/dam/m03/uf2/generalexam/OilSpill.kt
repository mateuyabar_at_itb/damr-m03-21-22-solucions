package cat.itb.mateuyabar.dam.m03.uf2.generalexam

data class OilSpill(val name: String, val company: String,
                    val litters:Int, val toxicity: Double,
) {
    val gravity get() = toxicity * litters
}