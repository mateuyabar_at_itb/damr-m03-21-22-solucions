package cat.itb.mateuyabar.dam.m03.uf1.iterative

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    var playerName = ""
    var playerScore = 0
    var winnerName = ""
    var winnerScore = 0
//    do{
//        playerName = scanner.nextLine()
//        if(playerName!="END"){
//            playerScore = scanner.nextLine().toInt()
//            if(playerScore>winnerScore){
//                winnerScore = playerScore
//                winnerName = playerName
//            }
//        }
//    }while(playerName!="END")

    playerName = scanner.nextLine()
    while(playerName!="END"){
        playerScore = scanner.nextLine().toInt()
        if(playerScore>winnerScore){
            winnerScore = playerScore
            winnerName = playerName
        }
        playerName = scanner.nextLine()
    }

    println("$winnerName: $winnerScore")
}