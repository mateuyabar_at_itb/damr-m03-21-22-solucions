package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val boxes = List(4){MutableList(4){0} }

    var i = scanner.nextInt()
    while(i!=-1){
        val j = scanner.nextInt()
        boxes[i][j]++
        i = scanner.nextInt()
    }

    println(boxes)
}