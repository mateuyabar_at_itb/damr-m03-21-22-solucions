package cat.itb.mateuyabar.dam.m03.uf1.seleccio.exam

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val speed = scanner.nextDouble()

    if(speed>140)
        println("Multa greu")
    else if(speed>120)
        println("Multa lleu")
    else
       println("Correcte")
}