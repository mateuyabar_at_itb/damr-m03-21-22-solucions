package cat.itb.mateuyabar.dam.m03.uf4.exercices.figures

class RectangleFigure(color: String, val width: Int, val height: Int):Figure(color) {
    override fun paintDots() {
        repeat(height){
            repeat(width){
                print("X")
            }
            println()
        }
    }
}