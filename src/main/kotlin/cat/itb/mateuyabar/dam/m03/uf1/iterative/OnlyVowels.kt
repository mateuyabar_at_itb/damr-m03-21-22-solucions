package cat.itb.mateuyabar.dam.m03.uf1.iterative

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val count = scanner.nextInt()

    // for(i in 1..conut){
    // for(i in 0..count-1){
    repeat(count){
        val letter = scanner.next()
        when(letter) {
            "a", "e", "i", "o", "u" -> print("$letter ")
        }
    }

}