package cat.itb.mateuyabar.dam.m03.uf5.exercices.lists

import cat.itb.mateuyabar.dam.m03.uf4.exercices.carpentryshop.Llisto
import cat.itb.mateuyabar.dam.m03.uf4.exercices.carpentryshop.readProducts
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val products = readProducts(scanner)
    val llistoPrice = products
        .filter { it is Llisto }
        .sumOf { it.totalPrice }
    println("Preu total llistons: $llistoPrice")

//    val maxPrice = products.maxOf { it.totalPrice }
//    val maxPriceProduct = products.find { it.totalPrice == maxPrice }
    val maxPriceProduct = products.maxByOrNull { it.totalPrice }
    println("Producte més car té una llargada de : ${maxPriceProduct?.lenght}")

    val moreThan100Count = products.count{ it.totalPrice > 100 }
    println("Num productés de més de 100€: $moreThan100Count")
}