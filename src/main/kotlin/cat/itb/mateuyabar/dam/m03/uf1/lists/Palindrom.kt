package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val word = scanner.next()
    //val palindrom = word.reversed()==word
    var palindrom = true
    for(i in 0..(word.lastIndex/2)) {
        if (word[i] != word[word.lastIndex - i]) {
            palindrom = false
            break
        }
    }

    println(palindrom)

}