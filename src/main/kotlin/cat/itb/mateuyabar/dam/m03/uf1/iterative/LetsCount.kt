package cat.itb.mateuyabar.dam.m03.uf1.iterative

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val count = scanner.nextInt()
    repeat(count){
        print(it+1)
    }
}