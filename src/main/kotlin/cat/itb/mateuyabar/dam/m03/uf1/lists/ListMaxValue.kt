package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*
import kotlin.math.max

fun main() {
    val scanner = Scanner(System.`in`)
    val list = readIntList(scanner)

    var max = list[0]
    for(value in list){
        if(value>max)
            max = value
//      Alternativa: max = max(max, value)
    }
    println(max)

//    list.maxOrNull()
}