package cat.itb.mateuyabar.dam.m03.uf5.exercices.tads.maps

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val votes : MutableMap<String, Int> = mutableMapOf()
    val name = scanner.nextLine()
    while(name!="END"){
        val currentVotes = votes[name] ?: 0
        votes[name] = currentVotes + 1
    }
    for((name, votes) in votes){
        println("$name: $votes")
    }
}