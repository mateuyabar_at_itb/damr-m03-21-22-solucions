package cat.itb.mateuyabar.dam.m03.uf1.data

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val value = scanner.nextInt()
    val plusOne = value + 1
    println("Després ve el $plusOne")
}