package cat.itb.mateuyabar.dam.m03.uf2.generalexam

fun countOnesRec(num : Int):Int {
    if(num<10){
        return if(num == 1) 1 else 0
    }
    val lastDigit = num % 10
    val ones = if (lastDigit==1) 1 else 0
    val withoutLastDigit = num /10
    return ones + countOnesRec(withoutLastDigit)
}