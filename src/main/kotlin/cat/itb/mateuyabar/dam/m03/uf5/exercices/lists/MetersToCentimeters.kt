package cat.itb.mateuyabar.dam.m03.uf5.exercices.lists

import cat.itb.mateuyabar.dam.m03.uf1.lists.readIntList
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val distancesInMeters = readIntList(scanner)
    distancesInMeters
        .map{ it*100 }
        .forEach{ println(it) }
}