package cat.itb.mateuyabar.dam.m03.uf1.seleccio

import cat.itb.mateuyabar.dam.m03.uf1.introfunctions.areaOfCircle
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val pizzaDiameter = scanner.nextDouble()
    val roundPizza = areaOfCircle(pizzaDiameter/2)

    val pizzaHeight = scanner.nextDouble()
    val pizzaWidth = scanner.nextDouble()
    val areaRectanglePizza = pizzaHeight*pizzaWidth

    if(roundPizza>areaRectanglePizza)
        println("Compra la rodona")
    else
        println("Compra la rectangular")
}