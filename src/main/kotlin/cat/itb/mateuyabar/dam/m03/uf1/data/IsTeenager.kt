package cat.itb.mateuyabar.dam.m03.uf1.data

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val age = scanner.nextInt()
    val isTeenager = age in 10..20
    println(isTeenager)
}