package cat.itb.mateuyabar.dam.m03.uf3

import java.nio.file.Path
import java.time.LocalDateTime
import kotlin.io.path.copyTo
import kotlin.io.path.createDirectories

fun main() {
    val homePath  = System.getProperty("user.home");
    val profilePath = Path.of(homePath, ".profile")

    val date = LocalDateTime.now().toString()
    val destinationFolder = Path.of(homePath, "backup", date)
    destinationFolder.createDirectories()
    val destination = destinationFolder.resolve(".profile")
    profilePath.copyTo(destination)
}