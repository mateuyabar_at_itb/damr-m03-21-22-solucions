package cat.itb.mateuyabar.dam.m03.uf3

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json


@kotlinx.serialization.Serializable
data class Rectangle(val width: Double, val height: Double){
    val area get() = width*height
}

fun main() {
    val json = Rectangle::class.java.getResource("/rectangle.json").readText()
    val rectangles : List<Rectangle> = Json.decodeFromString(json)
    printRectangles(rectangles)
}

fun printRectangles(rectangles: List<Rectangle>) {
    for(rectangle in rectangles){
        printRectangle(rectangle)
    }

}

fun printRectangle(rectangle: Rectangle) {
    println("Un rectangle de ${rectangle.width} x ${rectangle.height} té ${rectangle.area} d'area.")

}
