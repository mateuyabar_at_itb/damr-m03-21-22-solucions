package cat.itb.mateuyabar.dam.m03.uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val values = List(10){scanner.nextInt()}
    var minValue = Int.MAX_VALUE
//    for(i in 0..values.lastIndex){
//        if(values[i]<minValue){
//            minValue = values[i]
//        }
//    }
    /*for(value in values){
        if(value<minValue){
            minValue = value
        }
    }*/
    /*values.forEach{
        if(it<minValue){
            minValue = it
        }
    }*/
    values.forEach{ value->
        if(value<minValue){
            minValue = value
        }
    }

    val value = values.minOrNull()!!
    print(minValue)

}