package cat.itb.mateuyabar.dam.m03.uf1.introfunctions

import java.util.*


fun rectangleArea(width: Int, height: Int) = width*height


fun main() {
    val scanner = Scanner(System.`in`)
    val height = scanner.nextInt()
    val width = scanner.nextInt()
    val area = rectangleArea(width, height)
    println(area)
}