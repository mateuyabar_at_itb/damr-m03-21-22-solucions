package cat.itb.mateuyabar.dam.avent

import java.util.*
import kotlin.io.path.Path

fun main() {
    val scanner = Scanner(Path("data/aventofcode/1_1.txt"));
    while(scanner.hasNext()){
        val number = scanner.nextInt()
        println(number)
    }
}
