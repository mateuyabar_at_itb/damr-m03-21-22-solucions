package cat.itb.mateuyabar.dam

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.jupiter.api.Test

class ExposedTest {
    @Test
    fun test() {
        Database.connect("jdbc:h2:mem:test", driver = "org.h2.Driver")
        transaction {
            // print sql to std-out
            addLogger(StdOutSqlLogger)

            SchemaUtils.create(Cities)


            // insert new city. SQL: INSERT INTO Cities (name) VALUES ('St. Petersburg')
            val stPete = City.new {
                name = "St. Petersburg"
            }
            City.new {
                name = "Barcelona"
            }

            val bilbo = City.new {
                name = "Bilbao"
            }
            bilbo.name = "Bilbo"


            // 'select *' SQL: SELECT Cities.id, Cities.name FROM Cities
            println("Cities:")
            City.all().forEach(){println(it.name)}
            println("----")
            City.find{ Cities.name like "%B%"}.forEach(){ println(it.name) }


        }
    }

    object Cities : IntIdTable() {
        val name = varchar("name", 50)
    }

    class City(id: EntityID<Int>) : IntEntity(id) {
        companion object : IntEntityClass<City>(Cities)

        var name by Cities.name
    }
}