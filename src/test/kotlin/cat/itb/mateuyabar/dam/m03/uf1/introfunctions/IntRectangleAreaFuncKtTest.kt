package cat.itb.mateuyabar.dam.m03.uf1.introfunctions

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class IntRectangleAreaFuncKtTest {

    @Test
    fun rectangleArea() {
        assertEquals(20, rectangleArea(5,4))
    }

    @Test
    fun `rectangle area when 0 width`() {
        assertEquals(0, rectangleArea(0,4))
    }
}