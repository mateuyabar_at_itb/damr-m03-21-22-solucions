package cat.itb.mateuyabar.dam.m03.uf2.classfun

import com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOutNormalized
import com.github.stefanbirkner.systemlambda.SystemLambda.withTextFromSystemIn
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*


internal class CampSiteTest {
    val campSite = CampSite()


    @Test
    fun checkInTest() {
        campSite.checkIn("Joan", 2)
        campSite.checkIn("Maria", 7)

        assertEquals(campSite.slots, 2)
        assertEquals(campSite.personAmount, 9)
    }

    @Test
    fun checkOut() {
        campSite.checkIn("Joan", 2)
        campSite.checkIn("Maria", 7)
        campSite.checkIn("Pere", 10)
        campSite.checkOut("Maria")

        assertEquals(campSite.slots, 2)
        assertEquals(campSite.personAmount, 12)
    }

    @Test
    fun mainTest(){
        val input = """ENTRA 2 Maria
ENTRA 5 Mar
MARXA Maria
END"""

        val desired = """parcel·les: 1
persones: 2
parcel·les: 2
persones: 7
parcel·les: 1
persones: 5
"""
        withTextFromSystemIn(input).execute{
            val output = tapSystemOutNormalized{
                campSiteOrzanizerMain()
            }
            assertEquals(desired, output)
        }
    }
}


