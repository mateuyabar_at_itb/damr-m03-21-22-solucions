package cat.itb.mateuyabar.dam.m03.uf2.classfun

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll

internal class RightTriangleTest {
    private val rectangle = RightTriangle(2.0,4.0)

    @Test
    fun getArea() {
        val area = rectangle.area
        assertEquals(4.0, area, 0.00001)
    }

    @Test
    fun getPerimeter() {
        val perimeter = rectangle.perimeter
        assertEquals(10.4721, perimeter, 0.001)
    }

    @Test
    fun getHypothen() {
        val hypothen = rectangle.hypothen
        assertEquals(4.4721, hypothen, 0.001)
    }

    @Test
    fun toOutput() {
        val output = rectangle.toOutput()
        assertEquals("Un triangle de 2.0 x 4.0 té 4.0 d'area i 10.47213595499958 de perímetre.", output)
    }
}