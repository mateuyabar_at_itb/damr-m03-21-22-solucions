package cat.itb.mateuyabar.dam.m03.uf2.classfun

import com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOutNormalized
import com.github.stefanbirkner.systemlambda.SystemLambda.withTextFromSystemIn
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class ThreeInARowTest {

    @Test
    fun boardToStringTest() {
        val threeInARow = ThreeInARow(listOf(
            mutableListOf(0,1,2),
            mutableListOf(0,0,0),
            mutableListOf(2,1,2)))
        val output = threeInARow.boardToString()
        val desired = """· X 0
            |· · ·
            |0 X 0
        """.trimMargin()
        assertEquals(desired, output)
    }

    @Test
    fun playAtTest() {
        val threeInARow = ThreeInARow()
        threeInARow.playAt(0,0)
        threeInARow.playAt(2,2)
        threeInARow.playAt(1,2)

        assertEquals(1, threeInARow.valueAt(0,0))
        assertEquals(2, threeInARow.valueAt(2,2))
        assertEquals(1, threeInARow.valueAt(1,2))
    }
    @Test
    fun winnerByDiagonalTest() {
        val threeInARow = ThreeInARow(
            listOf(
                mutableListOf(0, 1, 2),
                mutableListOf(0, 2, 0),
                mutableListOf(2, 1, 1)
            )
        )
        assertEquals(2, threeInARow.winner())
    }
    @Test
    fun winnerByRowTest() {
        val threeInARow = ThreeInARow(
            listOf(
                mutableListOf(0, 1, 1),
                mutableListOf(2, 2, 2),
                mutableListOf(2, 1, 1)
            )
        )
        assertEquals(2, threeInARow.winner())
    }
    @Test
    fun winnerByColumnTest() {
        val threeInARow = ThreeInARow(
            listOf(
                mutableListOf(0, 1, 1),
                mutableListOf(2, 2, 1),
                mutableListOf(2, 1, 1)
            )
        )
        assertEquals(1, threeInARow.winner())
    }
    @Test
    fun winnerWhenNoWinnerTest() {
        val threeInARow = ThreeInARow(
            listOf(
                mutableListOf(0, 1, 1),
                mutableListOf(2, 2, 0),
                mutableListOf(2, 1, 1)
            )
        )
        assertEquals(null, threeInARow.winner())
    }
    @Test
    fun winnerWhenEmptyBoardTest() {
        val threeInARow = ThreeInARow(
            listOf(
                mutableListOf(0, 0, 0),
                mutableListOf(0, 0, 0),
                mutableListOf(0, 0, 0)
            )
        )
        assertEquals(null, threeInARow.winner())
    }

    @Test
    fun fullBoardTest() {
        val threeInARow = ThreeInARow(
            listOf(
                mutableListOf(1, 1, 1),
                mutableListOf(2, 2, 2),
                mutableListOf(2, 1, 1)
            )
        )
        assertEquals(true, threeInARow.fullBoard())
    }

    @Test
    fun fullBoardWhenNotFullTest() {
        val threeInARow = ThreeInARow(
            listOf(
                mutableListOf(1, 1, 1),
                mutableListOf(2, 2, 2),
                mutableListOf(2, 1, 0)
            )
        )
        assertEquals(false, threeInARow.fullBoard())
    }

    @Test
    fun mainTest(){

        val input = "0 0\n" +
                "1 1\n" +
                "0 1\n" +
                "2 2\n" +
                "0 2\n"
        val desiredOutput = """X · ·
· · ·
· · ·
X · ·
· 0 ·
· · ·
X X ·
· 0 ·
· · ·
X X ·
· 0 ·
· · 0
X X X
· 0 ·
· · 0
guanyen les X
"""
        withTextFromSystemIn(input).execute{
            val output = tapSystemOutNormalized{
                threeInARow()
            }
            assertEquals(desiredOutput, output)
        }
    }
}