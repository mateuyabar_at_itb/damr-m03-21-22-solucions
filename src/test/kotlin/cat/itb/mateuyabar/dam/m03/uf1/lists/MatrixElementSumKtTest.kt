package cat.itb.mateuyabar.dam.m03.uf1.lists

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class MatrixElementSumKtTest {

    @Test
    fun sumMatrixTest() {
        val sum = sumMatrix(listOf(listOf(1,2)))
        assertEquals(3, sum)
    }
}