package cat.itb.mateuyabar.dam.m03.uf1.introfunctions

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class IsTeenagerFuncKtTest {

    @Test
    fun `isATeenager returns true when teenager`() {
        assertTrue(isATeenager(15))
    }

    @Test
    fun `isATeenager returns false when older than 20`() {
        assertFalse(isATeenager(35))
    }

    @Test
    fun `isATeenager returns false when younger than 20`() {
        assertFalse(isATeenager(5))
    }

    @Test
    fun `isATeenager returns true when 10 years`() {
        assertTrue(isATeenager(10))
    }
//    @Test
//    fun `isATeenager returns false when 20 years`() {
//        assertFalse(isATeenager(20))
//    }

}