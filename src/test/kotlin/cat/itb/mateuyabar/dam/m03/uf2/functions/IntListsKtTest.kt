package cat.itb.mateuyabar.dam.m03.uf2.functions

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class IntListsKtTest {

    @Test
    fun minTest() {
        val result = min(listOf(5,9,5,8,10,6,2,99,25))
        assertEquals(2, result)
    }

    @Test
    fun minWithNegativeTest() {
        val result = min(listOf(5,9,5,8,-10,6,2,99,25))
        assertEquals(-10, result)
    }

    @Test
    fun maxTest() {
        val result = max(listOf(5,9,5,8,10,6,2,99,25))
        assertEquals(99, result)
    }

    @Test
    fun avgTest() {
        val result = avg(listOf(10,20,30))
        assertEquals(20.0, result, 0.0001)
    }
}