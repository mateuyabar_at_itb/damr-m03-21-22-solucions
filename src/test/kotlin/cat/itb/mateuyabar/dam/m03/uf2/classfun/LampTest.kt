package cat.itb.mateuyabar.dam.m03.uf2.classfun

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class LampTest {
    val lamp = Lamp()

    @Test
    fun turnOn() {
        lamp.turnOn()
        assertEquals(true, lamp.turnedOn)
    }

    @Test
    fun turnOff() {
        lamp.turnOff()
        assertEquals(false, lamp.turnedOn)
    }

    @Test
    fun toggleWhenOff() {
        lamp.toggle()
        assertEquals(true, lamp.turnedOn)
    }

    @Test
    fun toggleWhenOn() {
        val lamp = Lamp(true)
        lamp.toggle()
        assertEquals(false, lamp.turnedOn)
    }

    @Test
    fun performTurnOn() {
        lamp.perform("TURN ON")
        assertEquals(true, lamp.turnedOn)
    }
}